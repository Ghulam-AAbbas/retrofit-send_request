package com.example.retrofit.repository

import com.example.retrofit.api.RetrofitInstance
import com.example.retrofit.model.Post
import okhttp3.FormBody
import retrofit2.Response
import java.time.ZoneId

class Repository {

    suspend fun getPost(): Response<Post> {
        return RetrofitInstance.api.getPost()
    }

    suspend fun getPost2(number: Int): Response<Post> {
        return RetrofitInstance.api.getPost2(number)
    }

    suspend fun getCustomPosts(userId: Int, sort: String, order: String): Response<List<Post>> {
        return RetrofitInstance.api.getCustomPosts(userId, sort, order)
    }

   suspend fun getCustomPosts2(userId: Int, options: Map<String,String>): Response<List<Post>>{
       return RetrofitInstance.api.getCustomPosts2(userId, options)
   }

    suspend fun pushPost(post: Post): Response<Post>{
        return RetrofitInstance.api.pushPost(post)
    }

    suspend fun pushPost2(userId: Int, id: Int, title: String, body: String): Response<Post>{
        return  RetrofitInstance.api.pushPost2(userId, id, title, body)
    }

}